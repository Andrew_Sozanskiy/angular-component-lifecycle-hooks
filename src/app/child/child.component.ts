import {
  Component,
  Input,
  Output,
  SimpleChange,
  OnChanges,
  OnInit,
  DoCheck,
  AfterContentInit,
  AfterContentChecked,
  AfterViewInit,
  AfterViewChecked,
  OnDestroy,
  EventEmitter
} from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent
  implements
    OnChanges,
    OnInit,
    DoCheck,
    AfterContentInit,
    AfterContentChecked,
    AfterViewInit,
    AfterViewChecked,
    OnDestroy {
  @Input()
  name;
  @Output()
  log: EventEmitter<string> = new EventEmitter();

  _onChangesCounter: number = 0;

  ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
    let changesMsgs: string[] = [];
    for (let propName in changes) {
      if (propName === 'name') {
        changesMsgs.push(
          `name changed to "${changes['name'].currentValue}" - child`
        );
      }
    }
    this.log.emit(`onChanges (${this._onChangesCounter++}): ${changesMsgs}`);
  }

  ngOnInit() {
    this.log.emit(`onInit - child`);
  }

  ngDoCheck() {
    this.log.emit(`doCheck - child`);
  }

  ngAfterContentInit() {
    this.log.emit(`afterContentInit - child`);
  }

  ngAfterContentChecked() {
    this.log.emit(`afterContentChecked - child`);
  }

  ngAfterViewInit() {
    this.log.emit(`afterViewInit - child`);
  }

  ngAfterViewChecked() {
    this.log.emit(`afterViewChecked - child`);
  }

  ngOnDestroy() {
    this.log.emit(`onDestroy - child`);
  }
}
